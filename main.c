/*
 * File:   main.c
 * Author: Dmytro Faizullin
 *
 * Created on 01 August 2017, 09:37
 */
#pragma config FOSC = HS        // Oscillator Selection bits (HS oscillator)
#pragma config WDTE = OFF       // Watchdog Timer Enable bit (WDT disabled)
#pragma config PWRTE = OFF      // Power-up Timer Enable bit (PWRT disabled)
#pragma config CP = OFF         // FLASH Program Memory Code Protection bits (Code protection off)
#pragma config BOREN = OFF      // Brown-out Reset Enable bit (BOR disabled)
#pragma config LVP = OFF        // Low Voltage In-Circuit Serial Programming Enable bit (RB3 is digital I/O, HV on MCLR must be used for programming)
#pragma config CPD = OFF        // Data EE Memory Code Protection (Code Protection off)
#pragma config WRT = OFF        // FLASH Program Memory Write Enable (Unprotected program memory may not be written to by EECON control)

#define _XTAL_FREQ     20000000

#include <xc.h>
#include "TK-02-02-01_IF_PV.h" // IF_PV pin definitions.
//#include "TenkouDevPIC.h"

void solar_adc_read(unsigned char mode); // Defined in solar_adc.c

unsigned char data_1[96] @ 0x0110;//= {0}; // It is big - put it in a convenient place in RAM.
unsigned char data_2tx[65] @ 0x0190;//!!! change from [21] to [24];= {0}; // Re-use for gyro and magnetometer readings
unsigned char data_OBC[10];//,data_tx[63];

//unsigned int i2c_interrupt_status=0;

sd_communication_information_t sd_condition_1;
unsigned char part_num;
unsigned char i2c_interrupt_status=0;
unsigned long int addr_part;
extern unsigned char sector_status, number_of_sectors;
extern unsigned long int measurement_num, data_num, sector_num, first_in_dataset, mission_number;

//unsigned long int temp1;

void interrupt globalInterrupt(){
    data_2tx[0] = 15;
    for (unsigned char i=1;i<=15;i++){
        data_2tx[i] = i2c_interrupt_status;
    }
    i2cInterrupt(data_OBC, data_2tx);
    i2c_interrupt_status++;
}

void main(void)
{
    data_2tx[0] = 3;
    for (unsigned char i=1;i<=10;i++){
        data_2tx[i] = 0xAA;
    }
    SPI_init_pin();
    decoder_initialize();
    spiMasterInit();
    uartInit(UART_BAUD); // Initialise UART @ 9600 baud.
    MISO_SWITCH_INIT = 0;
    
    sd_condition_1.port = &PORTC;
    sd_condition_1.pin = 5;
    sd_condition_1.data_size = 96;
    sd_condition_1.start_byte_number = 0;
        
    sd_setup_SPImode(&sd_condition_1);  
            
    unsigned char snseAddr_1 = 0x80;
    i2cSlaveInit(&snseAddr_1);
    
    while (1){
        if( data_OBC[data_OBC[0]] != 0 ) //was modified by Juanjo
        {
            uartWriteBytes("i2c:  ",5);
            print_data_1_array(0, 10, data_OBC);
            
            data_2tx[0] = 10;
            for (unsigned char i=1;i<=8;i++){
                data_2tx[i] = data_OBC[i];
            }
            data_2tx[9] = 0xAA;
            data_2tx[10] = 0xAA; 
            data_2tx[11] = 0xAA; 
                        
            __delay_ms(100);
            switch (data_OBC[1]){
                case 0x10: //service commands
                    switch (data_OBC[2]){
                        case 0x11: //erase status blocks of all sectors of SD card
                            erase_all_mem_round_stat_SD(); //then find new sector in SD for saving data and write init data to the first black of the sector
                            print_hex_long(sector_status);
                            uartWriteBytes(", ",2);
                            print_hex_long(first_in_dataset);
                            uartWriteBytes(", ",2);
                            print_hex_long(sector_num);
                            uartWriteBytes("\n\r",2);
                            break;
                        case 0x12: //init SD card recording                    
                            /*Step 2: Find a sector for saving new data*/
                            find_new_sector_SD();
                                print_hex_long(sector_status);
                                uartWriteBytes(", ",2);
                                print_hex_long(first_in_dataset);
                                uartWriteBytes(", ",2);
                                print_hex_long(sector_num);
                                uartWriteBytes("\n\r",2);
                            /*Step 3: Write intit data to Initialization part of the new sector*/
                            number_of_sectors = 0;    
                            write_init_data_SD();

                            /*Step 4: reset counters of the SD card*/
                            data_num=7558;   //should be 0 for FM
                            break;
                        case 0x13: //read and print status blocks of all sectors of SD card
                            read_all_mem_round_stat_SD();
                            break; 
                        case 0x14: //change step of saving data
                            break;
                    }
                    break;
                case 0x20:  //Tick
                    break;
                case 0x30:  //reading and saving data from sensors. Preparing data for DL 
                    print_hex_long(sector_num);uartWriteBytes(", ",2);
                    print_hex_long(data_num);uartWriteBytes(", ",2);
                    print_hex_long(mission_number);uartWriteBytes(": ",2);uartWriteBytes("\n\r",2);
                    for (part_num=0;part_num<4;part_num++){
                        addr_part = 512 + 4 * (7559 * sector_num + data_num) + part_num;
                        sd_condition_1.block_number = addr_part;
                        sd_condition_1.data_size = 96;
                        switch(part_num){
                            case 0: 
                            //Solar panels (96 bytes)
                                solar_adc_read((unsigned char) 0); //        read Solar panels
                                break;
                            case 1:
                            //Sun sensors (96 bytes)
                                solar_adc_read((unsigned char) 4);  //        read Sun sensors                    
                                break;
                            case 2:
                                //Status (OBC time, ...)
                                for (unsigned char i=0;i<96;i++){
                                    data_1[i]=0x22;
                                }
                                break;
                            case 3:
                                for (unsigned char i=0;i<24;i++){   //        saving mean values of each sun sensor
                                    data_2tx[i] = 0x55;
                                    data_1[i]=data_2tx[i];
                                }
                                MISO_SWITCH = 1;    //0 - MISO from ADS is disconected; 1 - MISO from ADS is conected
                            //Gyros (21 bytes)        
                                gyroscopes(data_2tx);   //input gyro function here. Save data in unsigned char array data_2[24];
            //                    uartWrite(&Gyro_id);
            //                    uartWriteBytes(data_22,21);

                                for (unsigned char i=0;i<21;i++){
                                    data_2tx[i] = 0x66;
                                    data_1[24+i]=data_2tx[i];
                                }

                            //Magnetometer (12 bytes)     
//                                read_magnetometer(data_2tx); //input magnetometer function here. Save data in unsigned char array data_2[24];
            //                    uartWrite(&Magnetometer_id);
            //                    uartWriteBytes(data_22,12);

                                for (unsigned char i=0;i<10;i++){
                                    data_2tx[i] = 0x77;
                                    data_1[45+i]=data_2tx[i];
                                }
                                
                                data_2tx[0] = 64;
                                for (unsigned char i=1;i<9;i++){
                                    data_2tx[i] = data_OBC[i];
                                }
                                for (unsigned char i=0;i<55;i++){
                                    data_2tx[9+i] = data_1[i];
                                }
                                data_2tx[64] = 0xAA;
                                MISO_SWITCH = 0;    //0 - MISO from ADS is disconected; 1 - MISO from ADS is conected
                                break;
                        }
                        Write_SDcard(&sd_condition_1, data_1);
                        for (unsigned char i=0;i<96;i++){
                            data_1[i]=0x11;
                        }
                        Read_SDcard(&sd_condition_1, data_1);
                        print_hex_long(addr_part);uartWriteBytes(": ",2);
                        print_data_1_array(0, 64, data_1);
                    }

                    uartWriteBytes("\n\r",2);        
                    if (increment_addr_SD()){
                        write_init_data_SD();
                    }
                    
                    break;
                case 0x40: //prepare data for DL
//                    addr_part = 0;
//                    data_OBC[3] = 0x01;
//                    data_OBC[4] = 0x64;
//                    data_OBC[5] = 0x50;                    
//                    
//                    for (unsigned char i=0;i<3;i++){
//                        sd_condition_1.block_number = sd_condition_1.block_number + ((unsigned long int) data_OBC[i+3]) << ((2-i)*8);
//                    }
                    sector_num =data_OBC[3];
                    data_num = (((unsigned int)data_OBC[4]) << 8) + data_OBC[5];
                    part_num = data_OBC[6];
                    addr_part = 512 + 4 * (7559 * sector_num + data_num) + part_num;
                    

//                    addr_part = (((unsigned long int) data_OBC[3]) << 16) + (data_OBC[4] << 8) + data_OBC[5];
////
                    print_hex_long(addr_part);
                    uartWriteBytes("\n\r",2);
                    sd_condition_1.block_number = addr_part;
                    sd_condition_1.data_size = 96;
                    Read_SDcard(&sd_condition_1, data_1);
                    data_2tx[1] = data_OBC[1];
                    data_2tx[2] = data_OBC[2];
                    
                    print_hex_long(sector_num);uartWriteBytes(", ",2);
                    print_hex_long(data_num);uartWriteBytes(", ",2);
                    print_hex_long(part_num);uartWriteBytes(": ",2);   
                    
                    switch (data_OBC[2]){
                        case 0x41:  // prepare 1st half
                            data_2tx[0] = 51;
//                            data_tx[1] = data_OBC[1];
//                            data_tx[2] = data_OBC[2];
                            for (unsigned char i=0;i<48;i++){
                                data_2tx[i+3] = data_1[i];//*access];
                            }
                            break;
                        case 0x42:  // prepare 2nd half
                            data_2tx[0] = 50;
//                            data_tx[1] = data_OBC[1];
//                            data_tx[2] = data_OBC[2];
                            for (unsigned char i=0;i<48;i++){
                                data_2tx[i+3] = data_1[i+48];//*access];
                            }
                            break;
                        case 0x43:  // prepare status
                            data_2tx[0] = 13;
//                            data_tx[1] = data_OBC[1];
//                            data_tx[2] = data_OBC[2];
                            for (unsigned char i=0;i<10;i++){
                                data_2tx[i+3] = data_1[i];
                            }
                            break;
                        case 0x44:  // all sensors
                            data_2tx[0] = 58;
//                            data_tx[1] = data_OBC[1];
//                            data_tx[2] = data_OBC[2];
                            for (unsigned char i=0;i<55;i++){
                                data_2tx[i+3] = data_1[i];
                            }
                            break;
                        case 0x45:
                            break;
                    }
                    break;
            }
        }
        for (unsigned char i=0;i<9;i++)
            data_OBC[i] = 0; 
//        print_hex_short(i2c_interrupt_status);
//        print_data_1_array(0, 5, data_OBC);
//        uartWriteBytes(" ",1);
//        __delay_ms(MAIN_LOOP_WAIT); // Wait for MAIN_LOOP_WAIT sec .
    }
}