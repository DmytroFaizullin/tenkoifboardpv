/* Ten-Koh TK-02-02-01 IF_PV PCB header file, which defines all the pins of the
 * included PIC.
 * 
 * Intended for use with PIC16F877 and version of the PCB newer than 1.0.9.
 */
#ifndef TK_02_02_01
#define	TK_02_02_01

/* Included libraries*/
#include "TenkouGPIO.h"
//#include "TK-02-02-01_IF_PV.h" // IF_PV pin definitions.
#include <stdint.h> //To use uint_16, etc.
#include <xc.h>
#include <stdlib.h>

#include "uart.h"
#include "spi_pins.h"
#include "sd.h"
#include "sd_management.h"
#include "adc.h"
#include "gyro.h"
#include "decoder.h"
#include "magnetometer.h"
#include "i2c_slave.h"

#define UART_BAUD 9600 // Bits per second.
#define MAIN_LOOP_WAIT 1000 // How long to wait after every main loop.
// Function declarations.

/*
 *******************************************************************************
 *  Pin definitions.
 *******************************************************************************
 */

// SPI Master pins and initialisation registers. Defined in SPI-specific repositories,
// here only for information.
/*#define SPI_MOSI_MASTER RB3 // OUT = MOSI - master out slave in. Data to slave
#define SPI_MISO_MASTER RB1 // IN = MISO - master in slave out. Data from slave
#define SPI_CK_MASTER   RB2 // OUT = SCK - SPI clock
#define SPI_MOSI_MASTER_INIT TRISB3
#define SPI_MISO_MASTER_INIT TRISB1
#define SPI_CK_MASTER_INIT TRISB2*/

// SPI chip selects for SOLAR PANELS.
#define SPI_SENSE_CS_SP2_2 RA5
#define SPI_SENSE_CS_SP2_4 RA4
#define SPI_SENSE_CS_SP1_2 RA3
#define SPI_SENSE_CS_SP2_3 RA2
#define SPI_SENSE_CS_SP1_3 RA1
#define SPI_SENSE_CS_SP1_6 RC0
#define SPI_SENSE_CS_SP1_5 RC1
#define SPI_SENSE_CS_SP2_5 RC2
#define SPI_SENSE_CS_SP1_1 RE0
#define SPI_SENSE_CS_SP2_1 RE1
#define SPI_SENSE_CS_SP2_6 RE2
#define SPI_SENSE_CS_SP1_4 RD0

#define SPI_SENSE_CS_SP2_2_INIT TRISA5
#define SPI_SENSE_CS_SP2_4_INIT TRISA4
#define SPI_SENSE_CS_SP1_2_INIT TRISA3
#define SPI_SENSE_CS_SP2_3_INIT TRISA2
#define SPI_SENSE_CS_SP1_3_INIT TRISA1
#define SPI_SENSE_CS_SP1_6_INIT TRISC0
#define SPI_SENSE_CS_SP1_5_INIT TRISC1
#define SPI_SENSE_CS_SP2_5_INIT TRISC2
#define SPI_SENSE_CS_SP1_1_INIT TRISE0
#define SPI_SENSE_CS_SP2_1_INIT TRISE1
#define SPI_SENSE_CS_SP2_6_INIT TRISE2
#define SPI_SENSE_CS_SP1_4_INIT TRISD0

// UART interface.
#define UART_TX RC6 // Vice-versa, Tx on MCU.
#define UART_TX_INIT TRISC6 

// System-wide I2C, using the in-built PIC module.
#define I2C_SCL RC3
#define I2C_SDL RC4
#define I2C_SCL_INIT TRISC3
#define I2C_SDL_INIT TRISC4

// ADS interface.
#define SPI_SENSE_CS_GYRO RD1
#define SPI_SENSE_CS_ADS_CS_DEV1 RD4 // RC7 is RX line  so we changed it to RD4.
#define SPI_SENSE_CS_ADS_CS_DEV2 RD3
#define SPI_SENSE_CS_ADS_CS_DEV3 RD2
#define SPI_SENSE_CS_MAGADC RD5
#define MAG_SET RD6
#define MAG_RESET RD7

#define SPI_SENSE_CS_GYRO_INIT TRISD1
#define SPI_SENSE_CS_ADS_CS_DEV1_INIT TRISD4 // RC7 is RX line so we put iton RD4.
#define SPI_SENSE_CS_ADS_CS_DEV2_INIT TRISD3
#define SPI_SENSE_CS_ADS_CS_DEV3_INIT TRISD2
#define SPI_SENSE_CS_MAGADC_INIT TRISD5
#define MAG_SET_INIT TRISD6
#define MAG_RESET_INIT TRISD7

// CS for the SD card is actually defined in the
// SD sd_communication_information_t struct.
#define SPI_SENSE_CS_SD RC5
#define SPI_SENSE_CS_SD_INIT TRISC5

#define MISO_SWITCH RB5         //0 - MISO from ADS is disconected; 1 - MISO from ADS is conected
#define MISO_SWITCH_INIT TRISB5

void SPI_init_pin(void);

#endif	/* XC_HEADER_TEMPLATE_H */

