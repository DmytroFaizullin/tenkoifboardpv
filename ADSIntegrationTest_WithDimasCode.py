#!/home/alek/anaconda3/bin/python3

import serial
import time

# Human-readable names of the solar panels. The order in which they are printed
# by the PIC is according the port of the CS and then the pin number in that port.
solarPanelLabels={9:' 1-1:',2:' 1-2:',4:' 1-3:',8:' 1-4:',6:' 1-5:',5:' 1-6:',
				  10:' 2-1:',0:' 2-2:',3:' 2-3:',1:' 2-4:',7:' 2-5:',11:' 2-6:',}

def convertReadingToVolts(byte1,byte2):
	""" Convert two bytes read from a 12-bit ADC to a voltage in volts. Assume
	that the reference voltage was 5.0 V and that the data are right-aligned.
	"""
	return ( ((byte1 << 8) | byte2) & 0x0FFF ) * (5/4096)
	             
ser = serial.Serial('com15', 9600, timeout=0.8) # Open the serial port.

while True:
    data_ex = ser.read(97) # Parse the outputs coming from serial termianted by 0x0D i.e. \r

    if data_ex: # Read some valid data.
    # data_ex[0] is an identifier byte, tells us how to parse the following bytes
    # and what they actually mean.
    
        if (data_ex[0] == 0x01): # Gyroscope readings.

            """CONVERTING TEMPS"""
            temp_gyro_1 = data_ex[7]
            temp_gyro_2 = data_ex[14]
            temp_gyro_3 = data_ex[21]
        
            
            """CONVERTING GYRO 1 VALUES"""
            if (data_ex[1] & 0x80):
                    gyro_1_x = (~((data_ex[1] << 8) + data_ex[2]) + 1) & 0xFFFF
                    angle_velocity_gyro_1_x = -(gyro_1_x * 0.00875)
            else:
                    gyro_1_x = (data_ex[1] << 8) | data_ex[2]
                    angle_velocity_gyro_1_x = gyro_1_x * 0.00875
        
            if (data_ex[3] & 0x80):
                    gyro_1_y = (~((data_ex[3] << 8) + data_ex[4]) + 1) & 0xFFFF
                    angle_velocity_gyro_1_y = -(gyro_1_y * 0.00875)
            else:
                    gyro_1_y = ((data_ex[3] << 8) + data_ex[4])
                    angle_velocity_gyro_1_y = gyro_1_y * 0.00875
        
            if (data_ex[5] & 0x80):
                    gyro_1_z = (~((data_ex[5] << 8) + data_ex[6]) + 1) & 0xFFFF
                    angle_velocity_gyro_1_z = -(gyro_1_z * 0.00875) 
            else:
                    gyro_1_z = ((data_ex[5] << 8) + data_ex[6])
                    angle_velocity_gyro_1_z = gyro_1_z * 0.00875
        
        
            """CONVERTING GYRO 2 VALUES"""
            if (data_ex[8] & 0x80):
                    gyro_2_x = (~((data_ex[8] << 8) + data_ex[9]) + 1) & 0xFFFF
                    angle_velocity_gyro_2_x = -(gyro_2_x * 0.00875)
            else:
                    gyro_2_x = ((data_ex[8] << 8) + data_ex[9])
                    angle_velocity_gyro_2_x = gyro_2_x * 0.00875
        
            if (data_ex[10] & 0x80):
                    gyro_2_y = (~((data_ex[10] << 8) + data_ex[11]) + 1) & 0xFFFF
                    angle_velocity_gyro_2_y = -(gyro_2_y * 0.00875)
            else:
                    gyro_2_y = ((data_ex[10] << 8) + data_ex[11])
                    angle_velocity_gyro_2_y = gyro_2_y * 0.00875
        
            if (data_ex[12] & 0x80):
                    gyro_2_z = (~((data_ex[12] << 8) + data_ex[13]) + 1) & 0xFFFF
                    angle_velocity_gyro_2_z = -(gyro_2_z * 0.00875)
            else:
                    gyro_2_z = ((data_ex[12] << 8) + data_ex[13])
                    angle_velocity_gyro_2_z = gyro_2_z * 0.00875
        
        
            """CONVERTING GYRO 3 VALUES"""
            if (data_ex[15] & 0x80):
                    gyro_3_x = (~((data_ex[15] << 8) + data_ex[16]) + 1) & 0xFFFF
                    angle_velocity_gyro_3_x = -(gyro_3_x * 0.00875)
            else:
                    gyro_3_x = ((data_ex[15] << 8) + data_ex[16])
                    angle_velocity_gyro_3_x = gyro_3_x * 0.00875
        
            if (data_ex[17] & 0x80):
                    gyro_3_y = (~((data_ex[17] << 8) + data_ex[18]) + 1) & 0xFFFF
                    angle_velocity_gyro_3_y = -(gyro_3_y * 0.00875)
            else:
                    gyro_3_y = ((data_ex[17] << 8) + data_ex[18])
                    angle_velocity_gyro_3_y = gyro_3_y * 0.00875
        
            if (data_ex[19] & 0x80):
                    gyro_3_z = (~((data_ex[19] << 8) + data_ex[20]) + 1) & 0xFFFF
                    angle_velocity_gyro_3_z = -(gyro_3_z * 0.00875)
            else:
                    gyro_3_z = ((data_ex[19] << 8) + data_ex[20])
                    angle_velocity_gyro_3_z = gyro_3_z * 0.00875
                    
                    
            """PRINTING GYRO VALUES"""
            print("GYROX = ", '{0:.2f}'.format(angle_velocity_gyro_1_x), '{0:.2f}'.format(angle_velocity_gyro_2_x), '{0:.2f}'.format(angle_velocity_gyro_3_x), " DPS" )
            print("GYROY = ", '{0:.2f}'.format(angle_velocity_gyro_1_y), '{0:.2f}'.format(angle_velocity_gyro_2_y), '{0:.2f}'.format(angle_velocity_gyro_3_y), " DPS" )
            print("GYROZ = ", '{0:.2f}'.format(angle_velocity_gyro_1_z), '{0:.2f}'.format(angle_velocity_gyro_2_z), '{0:.2f}'.format(angle_velocity_gyro_3_z), " DPS" )
            print("TEMP = ", '{0:d}'.format(temp_gyro_1), '{0:d}'.format(temp_gyro_2), '{0:d}'.format(temp_gyro_3), " C" )
            print(" ")
        
        if (data_ex[0] == 0x02): # Magnetometer data.
            
            """CONVERTING MAGNETOMETERS"""
            mag_1x = ((data_ex[1] << 8) | (data_ex[2] + 1)) * 0.00007629
            mag_1y = ((data_ex[3] << 8) | (data_ex[4] + 1)) * 0.00007629
            mag_1z = ((data_ex[5] << 8) | (data_ex[6] + 1)) * 0.00007629
            
            ref_v1 = ((data_ex[7] << 8) | (data_ex[8] + 1)) * 0.00007629
            ref_v2 = ((data_ex[9] << 8) | (data_ex[10] + 1)) * 0.00007629
            ref_v3 = ((data_ex[11] << 8) | (data_ex[12] + 1)) * 0.00007629
            
            """PRINTING MAGNETOMETER VALUES"""
            print("MAGNETIC = ", '{0:.2f}'.format(mag_1x), '{0:.2f}'.format(mag_1y), '{0:.2f}'.format(mag_1z) )
            print("REF_VOLT = ", '{0:.2f}'.format(ref_v1), '{0:.2f}'.format(ref_v2), '{0:.2f}'.format(ref_v3) )
            print('{} bytes'.format(len(data_ex)))
           
        if (data_ex[0] == 0x03): # 12 solar panel ADCs' data.
         	print('SOLAR ADC ({} bytes) = '.format(len(data_ex)))
         	solarStr='' # Will append readings from each panel to this.
         	rdngCtr=0 # How many readings from the current solar panel have been read.
         	panelCtr=0 # Data from which panel is currently being read.
         	for j in range(1,len(data_ex)-1,2):
           	   	if rdngCtr==0 and panelCtr<=11: # Print a label before printing the 1st byte from every panel.
           	   	   	solarStr+=solarPanelLabels[panelCtr]
           	   	   	panelCtr+=1 # Label of the next panel.
           	   	
           	   	voltage=convertReadingToVolts(data_ex[j],data_ex[j+1]) # Convert bytes to volts.
           	   	solarStr+='{0:.2f},'.format(voltage)
           	   	rdngCtr+=1
           	   	if rdngCtr==4: # 4 readings per panel, other 4 are for the photodiode.
           	   	   	rdngCtr=0 # Next reading will be reading 0 from the next panel.
         	print(solarStr)
            	
        if (data_ex[0] == 0x04):
            try:
                print("SD CARD = ", data_ex[1:4].decode('ASCII'))
                print(" ")
            except UnicodeDecodeError as uerr:
                print(u'\uFFFD')
           
    
    #time.sleep(0.5)

