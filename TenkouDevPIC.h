#include "TenkouGPIO.h"
#include "TK-02-02-01_IF_PV.h" // IF_PV pin definitions.
#include <stdint.h> //To use uint_16, etc.
#include <xc.h>
#include <stdlib.h>

#include "uart.h"
#include "spi_pins.h"
#include "sd.h"
#include "sd_management.h"
#include "adc.h"
#include "gyro.h"
#include "decoder.h"
#include "magnetometer.h"
#include "i2c_slave.h"

#define UART_BAUD 9600 // Bits per second.
#define MAIN_LOOP_WAIT 1000 // How long to wait after every main loop.
// Function declarations.


