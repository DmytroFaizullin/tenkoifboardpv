/*
 * File:   TK-02-02-02_IF_PV.c
 * Author: oku-oa-61_2
 *
 * Created on May 16, 2018, 11:27 AM
 */

#include "TK-02-02-01_IF_PV.h"
#include <xc.h>

void SPI_init_pin(void){
    //TRIS
    SPI_SENSE_CS_SP2_2_INIT = 0;
    SPI_SENSE_CS_SP2_4_INIT = 0;
    SPI_SENSE_CS_SP1_2_INIT = 0;
    SPI_SENSE_CS_SP2_3_INIT = 0;
    SPI_SENSE_CS_SP1_3_INIT = 0;
    SPI_SENSE_CS_SP1_6_INIT = 0;
    SPI_SENSE_CS_SP1_5_INIT = 0;
    SPI_SENSE_CS_SP2_5_INIT = 0;
    SPI_SENSE_CS_SP1_1_INIT = 0;
    SPI_SENSE_CS_SP2_1_INIT = 0;
    SPI_SENSE_CS_SP2_6_INIT = 0;
    SPI_SENSE_CS_SP1_4_INIT = 0;
    SPI_SENSE_CS_GYRO_INIT = 0;
    SPI_SENSE_CS_MAGADC_INIT = 0;
    SPI_SENSE_CS_SD_INIT =0;
    
//    MISO_SWITCH_INIT = 0;
    
    SPI_SENSE_CS_SP2_2 = 1; 
    SPI_SENSE_CS_SP2_4 = 1;
    SPI_SENSE_CS_SP1_2 = 1;
    SPI_SENSE_CS_SP2_3 = 1;
    SPI_SENSE_CS_SP1_3 = 1;
    SPI_SENSE_CS_SP1_6 = 1;
    SPI_SENSE_CS_SP1_5 = 1;
    SPI_SENSE_CS_SP2_5 = 1;
    SPI_SENSE_CS_SP1_1 = 1;
    SPI_SENSE_CS_SP2_1 = 1;
    SPI_SENSE_CS_SP2_6 = 1;
    SPI_SENSE_CS_SP1_4 = 1;
    MAG_RESET_INIT=0;
    MAG_SET_INIT=0;
    
    SPI_SENSE_CS_SD = 1;
    
    SPI_SENSE_CS_GYRO = 1; 
    SPI_SENSE_CS_ADS_CS_DEV1 = 1; //changed from RC7 to RD4
    SPI_SENSE_CS_ADS_CS_DEV2 = 1;
    SPI_SENSE_CS_ADS_CS_DEV3 = 1;
    SPI_SENSE_CS_MAGADC = 1;
    MAG_SET = 0;
    MAG_RESET = 0;    
    
    ADCON1=0x06; // Set RA5 as digital pin to work as CS for the IF_SW ADC.
}