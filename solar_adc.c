#include "adc.h"

extern unsigned char data_1[96];
extern unsigned char data_2tx[65];

void solar_adc_read(unsigned char mode)    //0 - solar panels; 4 - sun sensor
/* Read the output of the IF_SW ADC and send it over UART to see if it's OK.
 * Use SPI_BUS_CS_EPS connected to SPI_EPS_CS_BATADC with jumpers. */
{            
    uint16_t data = 0; // Raw conversion results from the ADC.
    uint16_t data1 = 0;
    unsigned char j = 0;
    unsigned char i = 0;
    // Defines the CS for the ADC and its measurement settings.
    adc_communication_information_t trans_data;
    while ( j<12){
        if (j<5){
            trans_data.port = &PORTA;
            trans_data.pin = j+1;
        } else if (j<8){
            trans_data.port = &PORTC;
            trans_data.pin = j-5;
        } else if (j<9){
            trans_data.port = &PORTD;
            trans_data.pin = j-8;
        } else {
            trans_data.port = &PORTE;
            trans_data.pin = j-9;
        }
        trans_data.range_select = DOUBLE_RANGE;
        trans_data.outcoding_select = STRAIGHT_BINARY;
                
        // Read all ADC channels for Solar Cells.
        data1=0;
        for(i=0; i<4; i++){
            trans_data.channel_select = i + mode;
            data = adc_communication_normal(trans_data);
            //data = 10*j + i;
            data_1[j*8+2*i] = (data & 0x0F00)>>8;
            data_1[j*8+2*i+1] = (data & 0xFF);
            if (mode==4){
                data1 += data;
            }
        }
        if (mode==4){
            data1=data1/4;    
            data_2tx[j*2] = (data1 & 0x0F00)>>8;
            data_2tx[j*2+1] = (data1 & 0xFF);
        }    
        
        j++;
    }
}
